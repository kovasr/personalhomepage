<?
class DataBase {
    private $config = array (
        'host' => 'localhost',
        'user' => 'root',
        'password' => '',
        'database' => 'contacts',
    );

    private $connect;

    function __construct($config = null) {
        try {
            if (is_null($config)){
                $this->connect = new PDO("mysql:host=".$this->config['host'].";dbname=".$this->config['database'], $this->config['user'], $this->config['password']);
            }else{
                $this->connect = new PDO("mysql:host=".$config['host'].";dbname=".$config['database'], $config['user'], $config['password']);
            }
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            echo "Error DataBase!";
            die();
        }
    }
    function __destruct() {
        $this->connect = null;
    }

    public function query ($input_query, $data = NULL) {
        if (!is_null($data)){
            $sth = $this->connect->prepare($input_query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute($data);
            return $sth->fetchAll();
        }else{
            return $this->connect->query($input_query)->fetchAll();
        }
    }
}

class Messages {
    private $db;
    private $table = 'data'; //��������� �������� ������� � �� contacts
    private $config = array (
        'host' => 'localhost',
        'user' => 'root',
        'password' => '',
        'database' => 'contacts'
    );
    function __construct() {
        $this->db = new DataBase($config);
    }
    function __destruct() {
        $this->db = null;
    }
    function setMessage($message, $name, $email){
        $sql = "INSERT INTO $this->table SET message = :message, name = :name, email = :email";
        $data = array(':message' => $message, ':name' => $name, ':email' => $email);
        $this->db->query($sql, $data);
    }
}
?>